# Nodejs Express & Postgres Template

Nodejs Express & Postgres Template with `pg`, `db-migrate` and `express`.

- [db-migrate docs](https://db-migrate.readthedocs.io/en/latest/)
- [node-postgres docs](https://node-postgres.com/)
- [ejs docs](https://ejs.co/#docs)
- [express docs](https://expressjs.com/en/starter/hello-world.html)
